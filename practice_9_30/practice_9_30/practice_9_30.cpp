#include<iostream>
using namespace std;

//class Date
//{
//public:
//
//	//无参的构造函数
//	//Date() 
//	//{
//	//	_year = 2022;
//	//	_month = 10;
//	//	_day = 1;
//	//}
//
//	//有参的构造函数(缺省或非缺省)
//	//Date(int year,int month=1,int day=1)
//	//{
//	//	_year = year;
//	//	_month = month;
//	//	_day = day;
//	//}
//	
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//int main()
//{
//	//Date d1(2022,9,30);
//	//d1.Print();
//
//	Date d2;
//	d2.Print();
//}


///////////////////////////////////////////////////////
//class Time
//{
//public:
//	Time()
//	{
//		cout << "Time()" << endl;
//		_hour = 0;
//		_minute = 0;
//		_second = 0;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//	Date(int year, int month, int day = 6)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	// 基本类型(内置类型)
//	int _year = 2022;
//	int _month = 10;
//	int _day = 1;
//	// 自定义类型
//	Time _t;
//};
//int main()
//{
//	Date d(2023,5);
//	d.Print();
//	return 0;
//}



///////////////////////////////////////////////////
//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//int main()
//{
//	Date d;
//}

///////////////////////////////////////////////////////
typedef int DataType;
class Stack
{
public:
	Stack(size_t capacity = 3)
	{
		_array = (DataType*)malloc(sizeof(DataType) * capacity);
		if (NULL == _array)
		{
			perror("malloc申请空间失败!!!");
			return;
		}
		_capacity = capacity;
		_size = 0;
	}
	void Push(DataType data)
	{
		// CheckCapacity();
		_array[_size] = data;
		_size++;
	}
	// 其他方法...
	~Stack()
	{
		if (_array)
		{
			free(_array);
			_array = NULL;
			_capacity = 0;
			_size = 0;

		}
	}
private:
	DataType* _array;
	int _capacity;
	int _size;
};

//int main()
//{
//	Stack s;
//	s.Push(1);
//	s.Push(2);
//}



//class Time
//{
//public:
//	~Time()
//	{
//		cout << "~Time()" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//private:
//	// 基本类型(内置类型)
//	int _year = 1970;
//	int _month = 1;
//	int _day = 1;
//	// 自定义类型
//	Time _t;
//};

//int main()
//{
//	Date d;
//	return 0;
//}

/////////////////////////////////////////////////////////////////////////

class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	// Date(const Date& d)   // 正确写法
	Date(Date& d)   // 错误写法：编译报错，会引发无穷递归
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
		cout << "Data拷贝构造" << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

void func1(Date d)
{
	cout << "func1" << endl;
}

void func2(Date& d)
{
	cout << "func2" << endl;

}
int main()
{
	Date d1;
	//Date d2(d1);
	func1(d1);
	func2(d1);

	return 0;
}