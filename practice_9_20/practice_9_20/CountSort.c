#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void CountSort(int* arr, int sz)
{
	int min = arr[0], max = arr[0];
	for (int i = 1; i < sz; i++)
	{
		if (arr[i] > max)
			max = arr[i];
		if (arr[i] < min)
			min = arr[i];
	}

	int range = max - min + 1;
	int* count = (int*)malloc(sizeof(int) * range);
	if (count == NULL)
	{
		perror("malloc fail");
		return;
	}

	memset(count, 0, sizeof(int) * range);
	for (int i = 0; i < sz; i++)
	{
		count[arr[i] - min]++;
	}

	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (count[i]--)
			arr[j++] = i + min;
	}

	free(count);

}

int main()
{
	int arr[] = { 278,109,63,930,589,184,505,269,8,83 };
	int sz = sizeof(arr) / sizeof(arr[0]);

	CountSort(arr, sz);

	for (int i = 0; i < sz; i++)
		printf("%d ", arr[i]);
	printf("\n");
}