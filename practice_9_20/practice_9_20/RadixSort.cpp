#include<stdio.h>
#include<queue>
#include<iostream>
using namespace std;

#define K 3
#define RADIX 10

//定义基数
queue<int> Q[RADIX];

int GetKey(int value,int k)
{
	int key = 0;
	while (k>=0)
	{
		key = value % 10;
		value /= 10;
		k--;
	}
	return key;
}

//分发数据
void Distribute(int* arr, int sz, int k)
{
	for (int i = 0; i < sz; i++)
	{
		int key = GetKey(arr[i], k);
		Q[key].push(arr[i]);
	}
}

//回收数据
void Collect(int* arr)
{
	int k = 0;
	for (int i = 0; i < RADIX; i++)
	{
		while (!Q[i].empty())
		{
			arr[k++] = Q[i].front();
			Q[i].pop();
		}
	}
}

void RadixSort(int* arr, int sz)
{
	for (int i = 0; i < K; i++)
	{
		//分发数据
		Distribute(arr,sz, i);

		//回收数据
		Collect(arr);
	}
}

//int main()
//{
//	int arr[] = { 278,109,63,930,589,184,505,269,8,83 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	RadixSort(arr, sz);
//
//	for (int i = 0; i < sz; i++)
//		printf("%d ", arr[i]);
//	printf("\n");
//}