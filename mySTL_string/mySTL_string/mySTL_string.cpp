#include"mySTL_string.h"
#include<string>

void test_stl_string()
{
	//string s1("wml is handsome!");
	//cout << s1.capacity() << endl;
	//s1.reserve(5);
	//cout << s1.capacity() << endl;
	//s1.reserve(40);
	//cout << s1.capacity() << endl;

}

void test_mystring()
{
	//myString::string s;
	//s.push_back('a');
	//s.push_back('b');
	//s.push_back('c');
	//s.push_back('d');
	//s.push_back('e');

	myString::string s("abcdefghijk");
	size_t i = s.find("def");
	cout << i << endl;


	s.erase(0, 8);
	for (auto& str : s)
	{
		cout << str << " ";
	}
	cout << endl;


	s.insert(1, "wml");
	for (auto& str : s)
	{
		cout << str << " ";
	}
	cout << endl;


	s.append("ok");
	s.resize(5);
	//cout << s.capacity() << endl;
	for (auto& str : s)
	{
		cout << str << " ";
	}
	cout << endl;

	myString::string s2(s);
	cout << s2 << endl;

	s2 += "sb";
	cout << s2 << endl;

}

class Date
{
public:
	Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void Print()
	{
		cout << "Print()" << endl;
		cout << "year:" << _year << endl;
		cout << "month:" << _month << endl;
		cout << "day:" << _day << endl << endl;
	}
	 //Date* this
	 // const Date* this -->const修饰*this ,即this指向的对象不能被修改
	 // Date* const this -->const修饰this  ,即this指针不能被修改
	void Print() const
	{
		cout << "Print()const" << endl;
		cout << "year:" << _year << endl;
		cout << "month:" << _month << endl;
		cout << "day:" << _day << endl << endl;
	}
private:
	int _year; // 年
	int _month; // 月
	int _day; // 日
};
void Test()
{
	Date d1(2022, 1, 13);
	d1.Print();
	const Date d2(2022, 1, 13);//此处的const修饰Date对象，Date对象不能被修改
	d2.Print();
}

int main()
{
	//test_mystring();
	//test_stl_string();

	//Test();
	// 
	const myString::string s("abcde");
	myString::string::const_iterator it= s.begin();
	//const string s("abcd");
	//string::const_iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		//(*it)++;
		cout << *it << endl;
		it++;
	}
	return 0;
}