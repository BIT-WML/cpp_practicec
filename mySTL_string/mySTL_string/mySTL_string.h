#pragma once
#include<iostream>
#include<cassert>
using namespace std;

namespace myString
{
	class string {
	private:
		size_t _size;
		size_t _capacity;
		char* _str;
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		const static size_t npos = -1;


	public:
		string(const char* s = "")
		{
			_size = strlen(s);
			_capacity = _size;//此处的capacity不考虑结束的'\0'，表示可用的容量
			_str = new char[_capacity + 1];
			strcpy(_str, s);
		}

		string(const string& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			string tmp(s._str);//使用默认的构造函数
			swap(tmp);
		}

		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		iterator begin() { return _str; }
		iterator begin() const { return _str; }
		iterator end() { return _str + _size; }
		iterator end() const { return _str + _size; }
		size_t size() const { return _size; } 
		size_t capacity() const { return _capacity; }
		const char* c_str() const { return _str; }
		char& operator[](size_t pos) { assert(pos < _size); return _str[pos]; }//普通对象：可读可写
		const char& operator[](size_t pos) const { assert(pos < _size); return _str[pos]; }
		string& operator=(string s) { swap(s); return *this; }
		string& operator+=(char ch) { push_back(ch); return *this; }
		string& operator+=(const char* s) { append(s); return *this; }


		//不缩容
		void reserve(size_t newcapacity)
		{
			if (newcapacity > _capacity)
			{
				char* s = new char[newcapacity + 1];
				strcpy(s, _str);

				//释放原空间
				delete[] _str;
				_str = s;
				_capacity = newcapacity;
			}
		}

		//不缩容
		void resize(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
			else
			{
				_str[n] = '\0';
				_size = n;
			}
		}

		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}

		void push_back(char c)
		{
			if (_size == _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}
			_str[_size++] = c;
			_str[_size] = '\0';
		}

		void append(const char* s)
		{
			size_t len = strlen(s);
			if (_size + len > _capacity) reserve(_size + len);

			strcpy(_str + _size, s);
			_size += len;
		}


		//stl规定都是在pos之前进行插入
		string& insert(size_t pos, char ch)
		{
			assert(pos < _size);//此处_size如果为0,则直接断言错误,若assert(pos<=_size),则当pos==_size时,不满足在pos之前插入

			if (_size == _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : newcapacity * 2;
				reserve(newcapacity);
			}

			//注意边界检查以及隐式类型转换
			size_t end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}
			_str[pos] = ch;
			_size++;

			return *this;
		}

		string& insert(size_t pos, const char* s)
		{
			assert(pos < _size);
			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}

			size_t end = _size + len;
			while (end > pos + len - 1)
			{
				_str[end] = _str[end - len];
				--end;
			}
			strncpy(_str + pos, s, len);
			_size += len;

			return *this;
		}

		string& erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);

			if (len == npos || pos + len >= _size) { _str[pos] = '\0'; _size = pos; }
			else { strcpy(_str + pos, _str + pos + len); _size -= len; }

			return *this;
		}

		size_t find(char ch, size_t pos = 0) const
		{
			assert(pos < _size);
			while (pos < _size)
			{
				if (_str[pos] == ch) return pos;
				pos++;
			}
			return pos;
		}

		size_t find(const char* str, size_t pos = 0) const
		{
			assert(pos < _size);
			const char* ptr = strstr(_str + pos, str);
			if (ptr == nullptr)return npos;
			else return ptr - _str;
		}

		~string()
		{
			if (_str)
			{
				delete[] _str;
				_str = nullptr;
				_size = _capacity = 0;
			}
		}



	};

	ostream& operator<<(ostream& out, const string& s)
	{
		for (size_t i = 0; i < s.size(); i++) out << s[i];
		return out;
	}

	istream& operator>>(istream& in, string& s)
	{
		s.clear();
		char buffer[128] = "\0";
		size_t i = 0;
		char ch = in.get();
		while (ch != ' ' && ch != '\n')
		{
			if (i == 127) { s += buffer; i = 0; }
			buffer[i++] = ch;
			ch = in.get();
		}
		if (i > 0)
		{
			buffer[i] = '\0';
			s += buffer;
		}

		return in;
	}
}








