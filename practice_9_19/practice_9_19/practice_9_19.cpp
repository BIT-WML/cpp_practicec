#include<iostream>
using namespace std;

void TestRef()
{
    int a = 10;
    int& ra = a;//<====定义引用类型
    printf("%p\n", &a);
    printf("%p\n", &ra);
}

//void test()
//{
//    int a = 10;
//    int& ra = a;
//    int b = 20;
//    ra = b;
//    printf("%p\n%p", a, ra);
//}

int& test()
{
    int x = 0;
    x++;
    return x;
}



void a()
{
    int b = 20;
}

//int main()
//{
//    //TestRef();
//    int& ret = test();
//    printf("%d\n", ret);
//
//    printf("%d\n", ret);
//
//    a();
//    printf("%d\n", ret);
//
//    return 0;
//}


//int& Add(int a, int b)
//{
//    int c = a + b;
//    return c;
//}
//int main()
//{
//    int& ret = Add(1, 2);
//    cout << "Add(1, 2) is :" << ret << endl;
//    return 0;
//}

#include <time.h>
//struct A { int a[10000]; };
//void TestFunc1(A a) {}
//void TestFunc2(A& a) {}
//void TestRefAndValue()
//{
//    A a;
//    // 以值作为函数参数
//    size_t begin1 = clock();
//    for (size_t i = 0; i < 1000000; ++i)
//        TestFunc1(a);
//    size_t end1 = clock();
//    // 以引用作为函数参数
//    size_t begin2 = clock();
//    for (size_t i = 0; i < 1000000; ++i)
//        TestFunc2(a);
//    size_t end2 = clock();
//    // 分别计算两个函数运行结束后的时间
//    cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//    cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}


//int main()
//{
//    TestRefAndValue();
//}


//int main()
//{
//    int a = 10;
//    const int& ra = a;
//    a++;
//
//    const int b = 1;
//    const int& rb = b;
//}


//void func(const int& a)
//{
//    ;
//}
//
//int main()
//{
//    int a = 1;
//    const int b = 10;
//    func(b);
//    func(a);
//}


//int main()
//{
//
//    double a = 10.54;
//    // int& i = a;
//    int i = ((int)a)++;
//}


//int main()
//{
//    int a = 10;
//    int& ra = a;
//    ra = 10;
//
//    int* pa = &a;
//    *pa = 10;
//    return 0;
//}