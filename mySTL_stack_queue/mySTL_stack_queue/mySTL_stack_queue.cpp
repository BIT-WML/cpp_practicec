﻿#include"mySTL_queue.h"
#include"mySTL_stack.h"

#include<iostream>
#include"mySTL_priority_queue.h"
using namespace std;

int main()
{
	//myQueue::queue<int> q;
	//myStack::stack<int>  q;
	myPq::priority_queue<int> q;
	q.push(6);
	q.push(1);
	q.push(10);
	q.push(20);
	q.push(4);

	while(!q.empty())
	{
		cout << q.top() << " ";
		q.pop();
	}

	return 0;
}


