#pragma once
#include<deque>

namespace myStack
{
	template<class T,class Container = deque<T>>
	class stack
	{
	private:
		Container _con;

	public:
		void push(const T& x) { _con.push_back(x); }
		void pop() { _con.pop_back(); }
		const T& top() { return _con.back(); }
		bool empty() { return _con.empty(); }
		size_t size() { return _con.size(); }
	};
}