#pragma once
#include<vector>
#include<utility>
#include<iostream>

namespace myPq {

	template<class T>
	class less
	{
	public:
		bool operator()(const T& x, const T& y)const { return x < y; }
	};

	template<class T>
	class greater
	{
	public:
		bool operator()(const T& x, const T& y)const { return x > y; }
	};


	template<class T ,class Container = vector<T>,class Compare = less<T>>
	class priority_queue
	{
	private:
		Container _con;

	public:
		priority_queue(){}

		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
			:_con(first, last)
		{
			for (int i = (_con.size() - 1 - 1) / 2; i >= 0; i--)
				adjust_down(i);
		}

		void adjust_up(size_t child)
		{
			Compare com;
			size_t parent = (child - 1) / 2;
			while (child > 0)
			{
				if (com(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
					break;
			}
		}


		void push(const T& x)
		{
			_con.push_back(x);
			adjust_up(_con.size()-1);
		}

		void adjust_down(size_t parent)
		{
			Compare com;
			size_t child = parent * 2 + 1;
			while (child < _con.size())
			{
				if (child + 1 < _con.size() && com(_con[child], _con[child + 1]))
					child++;
				if (com(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 - 1;
				}
				else
					break;
			}
		}


		void pop()
		{
			std::swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjust_down(0);
		}

		int top() { return _con[0]; }
		bool empty() { return _con.empty(); }
		size_t size() { return _con.size(); }

	};
}


