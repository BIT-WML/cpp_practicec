#pragma once
#include<deque>


namespace myQueue
{
	template<class T,class Container = deque<T>>
	class queue
	{
	private:
		Container _con;

	public:
		void push(const T& x) { _con.push_back(x); }
		void pop() { _con.pop_front(); }
		const T& front() { return _con.front(); }
		bool empty() { return _con.empty(); }
		size_t size() { return _con.size(); }
	};
}
