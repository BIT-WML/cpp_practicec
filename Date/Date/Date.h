#pragma once
#include<iostream>
using namespace std;

class Date {
public:
	Date(int year = 2022,int month = 10,int day = 12)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	int getmonthday(int year,int month)
	{
		static int monthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if ((month == 2) && ((_year % 400 == 0) || (_year % 4 == 0 && _year % 100 != 0)))
		{
			return 29;
		}

		return monthDay[month];
	}

	void Print();

	Date& operator=(const Date& d);

	bool operator==(const Date& d) const;

	bool operator!=(const Date& d) const;

	bool operator>(const Date& d) const;

	bool operator>=(const Date& d) const;

	bool operator<(const Date& d) const;

	bool operator<=(const Date& d) const;

	Date& operator+=(int day);

	Date operator+(int day);

	Date& operator-=(int day);

	Date operator-(int day);

	//ǰ��--
	Date& operator--();

	//����--
	Date operator--(int);

	//ǰ��++
	Date& operator++();

	//����++
	Date operator++(int);

private:
	int _year;
	int _month;
	int _day;
};