#include"Date.h"

Date& Date::operator=(const Date& d)
{
	if (this != &d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	return *this;
}


void Date::Print()
{
	cout << _year << "-" << _month << "-" << _day << endl;
}

bool Date:: operator==(const Date& d) const
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

bool Date:: operator>(const Date& d) const
{
	if (_year > d._year)
		return true;
	else if (_year == d._year && _month > d._month)
		return true;
	else if (_year == d._year && _month == d._month && _day > d._day)
		return true;

	return false;
}


bool Date:: operator>=(const Date& d) const
{
	return *this > d || *this == d;
}

bool Date:: operator<(const Date& d) const
{
	//return !(*this >= d);
	return !(d <= *this);    
}

bool Date:: operator<=(const Date& d) const
{
	return !(*this > d);
}

bool Date:: operator!=(const Date& d) const
{
	return !(*this == d);
}


Date& Date:: operator+=(int day)
{
	_day += day;
	while (_day > getmonthday(_year,_month))
	{
		_day -= getmonthday(_year, _month);
		_month++;

		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}

Date Date::operator+(int day)
{
	Date temp(*this);
	temp += day;
	return temp;
}

Date& Date:: operator-=(int day)
{
	_day -= day;
	while (_day <= 0)
	{
		_month--;
		if (_month == 0)
		{
			_year--;
			_month = 12;
		}
		_day += getmonthday(_year, _month);
	}
	return *this;
}


Date Date:: operator-(int day)
{
	Date temp(*this);
	temp -= day;
	return temp;
}

//ǰ��--
Date& Date:: operator--()
{
	return (*this -= 1);
}

//����--
Date Date:: operator--(int)
{
	Date temp(*this);
	*this -= 1;
	return temp;
}

//ǰ��++
Date& Date:: operator++()
{
	return (*this += 1);
}

//����++
Date Date:: operator++(int)
{
	Date temp(*this);
	*this += 1;
	return temp;
}






