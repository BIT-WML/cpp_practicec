#include"Date.h"

int main()
{
	Date d1;
	d1.Print();

	Date d2(2022, 10, 1);
	d2.Print();

	int ret = (d1 == d2);
	cout << ret << endl;

	//d2+= 1000;
	//d2.Print();

	//(d2 + 1000).Print();
	//(d2 - 1000).Print();

	//(d2 -= 10000).Print();

	(--d2).Print();
	d2.Print();

	(d2--).Print();
	d2.Print();

	(++d2).Print();
	d2.Print();

	(d2++).Print();
	d2.Print();

}