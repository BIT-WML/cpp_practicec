#pragma once
#include<assert.h>
#include<iostream>
#include<vector>
#include<cmath>
#include<utility>
using namespace std;

template<class K,class V>
struct AVLTreeNode {

	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv;

	int _bf;//平衡因子

	AVLTreeNode(const pair<K,V>& kv):_kv(kv),_left(nullptr),_right(nullptr),_parent(nullptr),_bf(0) {}
};

template<class K,class V>
struct AVLTree
{
	typedef AVLTreeNode<K, V> Node;

private:
	Node* _root = nullptr;

public:
	bool Insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		
		//找到插入位置	
		while (cur)
		{
			if (cur->_kv.first < kv.first){ parent = cur; cur = cur->_right;}
			else if (cur->_kv.first > kv.first){ parent = cur; cur = cur->_left;}
			else return false;
		}

		//进行插入链接
		cur = new Node(kv);
		if (parent->_kv.first < kv.first){ parent->_right = cur; cur->_parent = parent;}
		else{ parent->_left = cur; cur->_parent = parent;}

		//更新平衡因子
		while (parent)
		{
			if (parent->_left == cur)  parent->_bf--;
			else  parent->_bf++;

			//调平衡
			//1.parent->_bf == 0 ，即插入之前_bf == 1 或者 -1 ，不需要继续更新 
			//2.parent->_bf == 1 或者 -1，即插入之前_bf == 0 ，高度变换了，需要继续往上更新
			//3.parnet->_bf == 2 或者 -1，即插入之前_bf == 1 或者 -1 ,需要调整
			if (parent->_bf == 0) break;
			else if (parent->_bf == 1 || parent->_bf == -1) { cur = parent; parent = parent->_parent; }
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				//旋转
				//1.左单旋
				//2.右单旋
				//3.左右双旋
				//4.右左双旋

				if (parent->_bf == 2 && cur->_bf == 1)   RotateL(parent);
				else if (parent->_bf == -2 && cur->_bf == -1)  RotateR(parent);
				else if (parent->_bf == 2 && cur->_bf == -1) RotateRL(parent);
				else if (parent->_bf == -2 && cur->_bf == 1) RotateLR(parent);
				else assert(false);
				break;
			}
			else assert(false);
		}
		return true;
	}


	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		//判断subRL是否为空，不为空才能链接其父节点
		if(subRL) subRL->_parent = parent;

		//激励parent的上一个结点，后续需要调整平衡因子
		Node* ppnode = parent->_parent;
		subR->_left = parent;
		//subR->_parent = parent->_parent;
		parent->_parent = subR;

		//parent可能是一个树的根节点，也可能是一颗子树
		if (ppnode == nullptr) { _root = subR; _root->_parent = nullptr; }
		else
		{
			//if (ppnode->_right == subR) ppnode->_right = subR;--------------my bug
			if (ppnode->_right == parent) ppnode->_right = subR;
			else ppnode->_left = subR;

			subR->_parent = ppnode;
		}

		subR->_bf = parent->_bf = 0;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR) subLR->_parent = parent;

		Node* ppnode = parent->_parent;
		subL->_right = parent;
		//不论parent->_parent是否为空都可以链接
		//subL->_parent = parent->_parent;
		parent->_parent = subL;

		if (ppnode == nullptr) { _root = subL; _root->_parent = nullptr; }
		else
		{
			if (ppnode->_left == parent) ppnode->_left = subL;
			else ppnode->_right = subL;

			subL->_parent = ppnode;
		}

		subL->_bf = parent->_bf = 0;
	}


	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		int bf = subLR->_bf;
		//RotateL(subL);
		//RotateR(subLR);
		RotateL(parent->_left);
		RotateR(parent);

		if (bf == -1)//subLR的左树新增结点
		{
			subLR->_bf = 0;
			subL->_bf = 0;
			parent->_bf = 1;
		}
		else if (bf == 1)//subLR的右树新增结点
		{
			parent->_bf = 0;
			subL->_bf = -1;
			subLR->_bf = 0;
		}
		else if (bf == 0)//subLR新增
		{
			subL->_bf = 0;
			subLR->_bf = 0;
			parent->_bf = 0;
		}
		else assert(false);
	}

	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		int bf = subRL->_bf;
		//RotateL(subR);
		//RotateR(subRL);
		RotateR(parent->_right);
		RotateL(parent);

		if (bf == 1)//subRL的右节点新增
		{
			subR->_bf = 0;
			subRL->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			subR->_bf = 1;
			subRL->_bf = 0;
			parent->_bf = 0;
		}
		else if (bf == 0)
		{
			subR->_bf = 0;
			subRL->_bf = 0;
			parent->_bf = 0;
		}
		else assert(false);

	}

	void Inorder() { _Inorder(_root); }
	bool isBalance() { return _isBalance(_root); }

	int Height(Node* root)
	{
		if (root == nullptr) return 0;
		int leftH = Height(root->_left);
		int rightH = Height(root->_right);

		return leftH > rightH ? leftH + 1 : rightH + 1;
	}


private:
	void _Inorder(Node* root)
	{
		if (root == nullptr) return;

		_Inorder(root->_left);
		cout << root->_kv.first << " : " << root->_kv.second << endl;
		_Inorder(root->_right);
	}

	bool _isBalance(Node* root)
	{
		if (root == nullptr) return true;

		int left = Height(root->_left);
		int right = Height(root->_right);

		if (right - left != root->_bf) { cout << "_bf error!\n"; return false; }
		return abs(right - left) < 2 && _isBalance(root->_left) && _isBalance(root->_right);
	}


};

void TestAVLTree()
{
	srand(time(0));
	const size_t N = 10000;
	AVLTree<int, int> t;

	for (size_t i = 0; i < N; ++i)
	{
		size_t x = rand();
		t.Insert(make_pair(x, x));
		//cout << t.IsBalance() << endl;
	}

	t.Inorder();

	//vector<int> arr = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	////vector<int> arr = { 16, 3, 7, 11, 9, 26 };
	//AVLTree<int, int> t;
	//for (int i = 0; i < arr.size(); i++)
	//{
	//	t.Insert(make_pair(arr[i], arr[i]));
	//}
	//cout << "hello" << endl;
	//t.Inorder();
	
	cout <<	t.isBalance() << endl;
}