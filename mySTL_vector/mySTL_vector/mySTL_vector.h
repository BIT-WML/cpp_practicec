#pragma once
#include<iostream>
#include<assert.h>
using namespace std;


namespace myVector
{
	template<class T>
	class vector {

	public:
		typedef T* iterator;
		typedef const T* const_iterator;
	private:
		iterator _start;
		iterator _finish;
		iterator _end_of_storage;

	public:
		iterator begin() { return _start; }
		iterator end() { return _finish; }
		const_iterator begin() const { return _start; }
		const_iterator end() const { return _finish; }
		T& operator[](size_t pos) { assert(pos < size()); return _start[pos]; }
		const T& operator[](size_t pos) const { assert(pos < size()); return _start[pos]; }
		bool empty() const { return _finish == _start; }
		size_t size() const { return _finish - _start; }
		size_t capacity() const { return _end_of_storage - _start; }
		void clear() { _finish = _start; }

		vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{}


		//vector<char> v1(10, 'A');
		explicit vector(int n, const T& val = T())
			: _start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			reserve(n);
			for (int i = 0; i < n; i++) push_back(val);
			cout << "vector(int n, const T& val = T())" << endl;
		}


		//vector<int> v1(10, 1);----->可能会匹配vector(InputIterator first, InputIterator last)
		 vector(size_t n,const T& val = T())
			: _start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)push_back(val);
			cout << "vector(size_t n,const T& val = T())" << endl;
		}

		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			: _start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
			cout << "vector(InputIterator first, InputIterator last)" << endl;
		}


		vector(const vector<T>& v)
			: _start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			vector<T> tmp(v.begin(), v.end());
			swap(tmp);
		}

		//利用了传值传参时的拷贝构造函数
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			//cout << "opeartor=" << endl;
			return *this;
		}

		~vector()
		{
			delete[] _start;
			_start = _finish = _end_of_storage = nullptr;
		}



		void reserve(size_t n)
		{
			if (n > capacity())
			{
				T* tmp = new T[n];
				size_t oldSize = size();
				if (_start)
				{
					for (size_t i = 0; i < oldSize; i++)
					{
						tmp[i] = _start[i];
					}
					delete[] _start;
				}

				_start = tmp;
				_finish = tmp + oldSize;
				//size() == _finish-_start == _finish-tmp
				//而原来的_finish已经被释放，造成了访问野指针
				_end_of_storage = _start + n;
			}
		}


		void resize(size_t n,T val = T())
		{
			if (n > capacity()) reserve(n);
			if (n > size())
			{
				while (_finish < _start + n)
				{
					*_finish = val;
					++_finish;
				}
			}
			else
				_finish = _start + n;
		}


		void push_back(const T& x )
		{
			if (_finish == _end_of_storage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}

			*_finish = x;
			_finish++;
		}

		void pop_back()
		{
			assert(!empty());
			--_finish;
		}

		iterator insert(iterator pos, const T& val)
		{
			assert(pos <= _finish);
			assert(pos >= _start);

			if (_finish == _end_of_storage)
			{
				//插入时扩容导致pos指针失效
				int len = pos - _start;
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
				pos = _start + len;
			}

			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}
			*pos = val;
			++_finish;
				
			return pos;
		}

		iterator erase(iterator pos)
		{
			assert(pos <= _finish);
			assert(pos >= _start);

			iterator begin = pos + 1;
			while (begin < _finish)
			{
				*(begin - 1) = *begin;
				++begin;
			}
			--_finish;

			return pos;
		}


		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_end_of_storage, v._end_of_storage);
		}

	};
}




