﻿#include"mySTL_vector.h"
#include<vector>

void test_vector6()
{
	// 要求删除所有偶数
	std::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
    v.push_back(5);

	std::vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			it = v.erase(it);
		}
		else
		{
			++it;
		}
	}

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

class Solution {
public:
		myVector::vector<myVector::vector<int>> generate(int numRows) {
		myVector::vector<myVector::vector<int>> vv;
		vv.resize(numRows);
		for (size_t i = 0; i < vv.size(); ++i)
		{
			vv[i].resize(i + 1, 0);
			vv[i][0] = vv[i][vv[i].size() - 1] = 1;
		}

		for (size_t i = 0; i < vv.size(); ++i)
		{
			for (size_t j = 0; j < vv[i].size(); ++j)
			{
				if (vv[i][j] == 0)
				{
					vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
				}
			}
		}

		return vv;
	}
};

int main()
{
	myVector::vector<myVector::vector<int>> vvRet = Solution().generate(5);

	for (size_t i = 0; i < vvRet.size(); ++i)
	{
		for (size_t j = 0; j < vvRet[i].size(); ++j)
		{
			cout << vvRet[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;

	   return 0;
}
//int main()
//{
//    //myVector::vector<int> v(1);
//    vector<int> v(1);
//    //v.reserve(100);
//
//    //v.push_back(1);
//    //v.push_back(2);
//    //v.push_back(3);
//    //v.push_back(4);
//    //v.push_back(5);
//    //v.pop_back();
//
//    //v.resize(10, -1);
//    //v.resize(3);
//
//    //v.insert(v.begin(), 100);
//    v.insert(v.end(), 200);
//
//    //插入指针后迭代器it失效
//    vector<int>::iterator it = find(v.begin(), v.end(), 2);
//    v.insert(it, 500);
//    //it++;
//    //*it;
//
//    //删除后迭代器也失效
//    vector<int>::iterator its = find(v.begin(), v.end(), 500);
//    v.erase(its);
//    //*its;
//    //it++;
//
//
//    for (auto  & it : v)
//    {
//        cout << it << " ";
//    }
//    cout << endl;

 //   test_vector6();

 //   myVector::vector<int> v1(10, 1);//vector(int n, const T& val = T())
 //   myVector::vector<char> v2(10, 'a');//vector(int n, const T& val = T())
 //   myVector::vector<char> v3(v1.begin(), v1.end());//vector(InputIterator first, InputIterator last)
	//myVector::vector<int> v4((size_t)10, 1); //vector(size_t n,const T& val = T())



	//vector<vector<int>> vvRet = Solution().generate(5);

	//for (size_t i = 0; i < vvRet.size(); ++i)
	//{
	//	for (size_t j = 0; j < vvRet[i].size(); ++j)
	//	{
	//		cout << vvRet[i][j] << " ";
	//	}
	//	cout << endl;
	//}
	//cout << endl;

 //   return 0;
//}

//void TestVectorExpand()
//{
//	size_t sz;
//	vector<int> v;
//	sz = v.capacity();
//	cout << "making v grow:\n";
//	for (int i = 0; i < 100; ++i)
//	{
//		v.push_back(i);
//		if (sz != v.capacity())
//		{
//			sz = v.capacity();
//			cout << "capacity changed: " << sz << '\n';
//		}
//	}
//}
//
//
//int main()
//{
//	TestVectorExpand();
//	return 0;
//}

//int main()
//{
//	vector<int> v{ 1,2,3,4,5,6 };
//
//	auto it = v.begin();
//	cout << *it << endl;
//	v.erase(it);
//	cout << *it << endl;
//	//v.insert(v.begin(), 0);
//	//while (it != v.end())
//	//{
//	//	cout << *it << endl;
//	//	it++;
//	//}
//	//return 0;
//}

