#include<iostream>
#include<vector>
#include<stack>
#include<stdio.h>

template<class T>
void swap(T& a, T& b)
{
	T t = a;
	a = b;
	b = t;
}

//冒泡排序
template<class T>
void bubble_sort(std::vector<T>& arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		int flag = 0;
		for (int j = 0; j < size - 1 - i; j++)
		{
			if (arr[j + 1] < arr[j])
			{
				swap(arr[j + 1], arr[j]);
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}
}

//选择排序
template<class T>
void select_sort(std::vector<T>& arr, int size)
{
	for (int i = 0; i < size ; i++)
	{
		int min = i;
		for (int j = i + 1; j < size ; j++)
		{
			if (arr[min] > arr[j])
				min = j;
		}
		if (min != i) swap(arr[min], arr[i]);
	}
}

//插入排序
template<class T>
void insert_sort(std::vector<T>& arr, int size)
{
	for (int i = 1; i < size; i++)
	{
		int key = arr[i];
		int end = i - 1;
		while (end >= 0 && key < arr[end])
		{
			arr[end +1] = arr[end];
			end--;
		}
		arr[end + 1] = key;
	}
}

//希尔排序
template<class T>
void shell_sort(std::vector<T>& arr, int size)
{
	int gap = size;
	while (gap > 1)
	{
		gap /= 2;

		for (int i = 0; i < size - gap; i++)
		{
			int end = i;
			int key = arr[end + gap];
			while (end >= 0 && key < arr[end])
			{
					arr[end + gap] = arr[end];
					end -= gap;
			}
			arr[end + gap] = key;
		}
	}
}

//堆排序
template<class T>
void AdjustDown(std::vector<T>& arr, int size, int parent)
{
	int child = parent * 2 + 1;
	while (child < size)
	{
		int max = child;
		if (child + 1 < size && arr[child + 1] > arr[child])
			max = child + 1;
		if (arr[parent] < arr[max])
		{
			swap(arr[max], arr[parent]);
			parent = max;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}

template<class T>
void heap_sort(std::vector<T>& arr, int size)
{
	for (int i = (size - 1 - 1) / 2; i >= 0; i--)
		AdjustDown(arr, size, i);

	for (int i = 1; i < size; i++)
	{
		swap(arr[0], arr[size - i]);
		AdjustDown(arr, size - i, 0);
	}
}

//快速排序的优化——三数取中（key）
template<class T>
int getKey(std::vector<T>& arr, int begin, int end)
{

	int mid = (begin + end) / 2;

	//24,2,56,13,97,45,32,101,66,5
	if (arr[begin] < arr[mid])
	{
		if (arr[mid] < arr[end])
			return mid;
		else if (arr[begin] > arr[end])
			return begin;
		else
			return end;

	}
	else
	{
		if (arr[mid] > arr[end])
			return mid;
		else if (arr[end] > begin)
			return begin;
		else
			return end;
	}
}

//快速排序
template<class T>
int partition1(std::vector<T>& arr, int left, int right)
{
	int mid = getKey(arr, left, right);
	swap(arr[left], arr[mid]);
	//left会移动，利用key保存最左边的下标；
	int key = left;

	while (left < right)
	{
		while (left < right && arr[right] >= arr[key])//往左找小
			--right;
		while (left < right && arr[left] <= arr[key]) //往右找大
			++left;
		if (left < right)
			swap(arr[left], arr[right]);
	}
	//从右边开始找可以确定相遇位置的值要小于key

	swap(arr[left], arr[key]);

	return left;
}

template<class T>
void quick_sort(std::vector<T>& arr, int begin, int end)
{
	if (begin >= end) return;

	int mid = partition1(arr, begin, end);

	quick_sort(arr, begin, mid - 1);
	quick_sort(arr, mid + 1, end);

}

template<class T>
void quick_sortNoR(std::vector<T>& arr, int left, int right)
{
	std::stack<T> s;
	s.push(left);
	s.push(right);

	while (!s.empty())
	{
		right = s.top();
		s.pop();
		left = s.top();
		s.pop();


		int div = partition1(arr, left, right);
		if (div + 1 < right)
		{
			s.push(div + 1);
			s.push(right);
		}
		if (left < div - 1)
		{
			s.push(left);
			s.push(div - 1);
		}

	}
}

//归并排序 对于[)区间
template<class T>
void merge(std::vector<T>& arr, int l, int mid, int r) {
	int index = 0;
	int ptrL = l;
	int ptrR = mid;
	static std::vector<T>tempary;
	if (arr.size() > tempary.size()) {
		tempary.resize(arr.size());
	}
	while (ptrL < mid && ptrR < r) {
		if (arr[ptrL] < arr[ptrR]) 
			tempary[index++] = arr[ptrL++];
		else
			tempary[index++] = arr[ptrR++];
	}
	while (ptrL < mid) {
		tempary[index++] = arr[ptrL++];
	}
	while (ptrR < r) {
		tempary[index++] = arr[ptrR++];
	}

	std::copy(tempary.begin(), tempary.begin() + index , arr.begin() + l);
}

template<class T>
void merge_sort(std::vector<T>& arr, int l, int r) { // sort the range [l, r) in arr
	if (r - l <= 1) return;
	int mid = (l + r) / 2;
	merge_sort(arr, l, mid);
	merge_sort(arr, mid, r);
	merge(arr, l, mid, r);

}

//归并排序 //对于[]区间
//template<class T>
//void merge(std::vector<T>& arr, int l, int mid, int r) {
//	int index = 0;
//	int ptrL = l;
//	int ptrR = mid + 1;
//	static std::vector<T>tempary;
//	if (arr.size() > tempary.size()) {
//		tempary.resize(arr.size());
//	}
//	//while (ptrL != mid && ptrR != r) {
//	// != 和 <= 在不同情况下效果不同！！！！
//	while (ptrL <= mid && ptrR <= r) {
//		if (arr[ptrL] < arr[ptrR]) 
//			tempary[index++] = arr[ptrL++];
//		else
//			tempary[index++] = arr[ptrR++];
//	}
//	while (ptrL <= mid) {
//		tempary[index++] = arr[ptrL++];
//	}
//	while (ptrR <= r) {
//		tempary[index++] = arr[ptrR++];
//	}
//
//	std::copy(tempary.begin(), tempary.begin() + index , arr.begin() + l);
//}
//
//template<class T>
//void merge_sort(std::vector<T>& arr, int l, int r) { // sort the range [l, r) in arr
//	if (r - l <= 0) return;
//	int mid = (l + r) / 2;
//	merge_sort(arr, l, mid);
//	merge_sort(arr, mid + 1, r);
//	merge(arr, l, mid, r);
//
//}

//归并非递归
template<class T>
void merge_sortNoR(std::vector<T>& arr, int sz)
{
	static std::vector<T> tempary;
	if (tempary.size() < arr.size())
		tempary.resize(arr.size());

	int gap = 1;
	while (gap < arr.size())
	{
		for (int i = 0; i < sz; i += 2 * gap)
		{
			int b1 = i, e1 = i + gap - 1;
			int b2 = i + gap, e2 = i + 2 * gap - 1;

			if (e1 >= sz) break;//第一组越界
			if (b2 >= sz) break;//第二组全部越界
			if (e2 >= sz) e2 = sz - 1;//第二组部分越界，继续归并

			//printf("[%d,%d][%d,%d]\n", b1, e1, b2, e2);

			int index = i;
			while (b1 <= e1 && b2 <= e2)
			{
				if (arr[b1] <= arr[b2])
					tempary[index++] = arr[b1++];
				else
					tempary[index++] = arr[b2++];
			}

			while (b1 <= e1)
			{
				tempary[index++] = arr[b1++];
			}
			while (b2 <= e2)
			{
				tempary[index++] = arr[b2++];
			}
			
			std::copy(tempary.begin(), tempary.begin() + index, arr.begin());

		}
		gap *= 2;
	}

}

int main()
{
	std::vector<int> arr{ 6,1,2,7,9,3,4,5,10,8 };

	//bubble_sort(arr, arr.size());
	//select_sort(arr, arr.size());
	//insert_sort(arr, arr.size());
	//shell_sort(arr, arr.size());
	//heap_sort(arr, arr.size());
	quick_sort(arr, 0, arr.size() - 1);
	//quick_sortNoR(arr, 0, arr.size() - 1);
	//merge_sort(arr, 0, arr.size()); //对于[)区间
	//merge_sort(arr, 0, arr.size() - 1); //对于[]区间
	//merge_sortNoR(arr, arr.size()); //对于[)区间

	for(auto n : arr)
	{
		std::cout << n << " ";
	}
	std::cout << std::endl;
}
