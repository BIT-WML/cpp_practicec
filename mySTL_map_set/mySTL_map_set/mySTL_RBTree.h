#pragma once
#include<utility>
#include<iostream>
using namespace std;

enum Color{ RED, BLACK };

template<class T>
struct RBTreeNode
{
	T _data;
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	Color _col;

	RBTreeNode(const T& data)
		:_data(data)
		,_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_col(RED)
	{}
};

template <class T,class Ref,class Ptr>
struct _RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef _RBTreeIterator<T,Ref,Ptr> Self;
	typedef _RBTreeIterator<T, T&, T*> iterator;
	Node* _node;

	_RBTreeIterator(Node* node) :_node(node) { }
	_RBTreeIterator(const iterator& s) :_node(s._node) {  }
	Ref operator*() { return _node->_data; }
	Ptr operator->() { return &_node->_data; }
	bool operator!=(const Self& s) const { return _node != s._node; }
	bool operator==(const Self& s) const { return _node == s._node; }

	Self& operator++()
	{
		if (_node->_right)//右边有子树
		{
			Node* min = _node->_right;
			while (min->_left) min = min->_left;
			_node = min;
		}
		else//右边无子树
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && cur == parent->_right)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}

	Self& operator--()
	{
		if (_node->_left)
		{
			Node* max = _node->_left;
			while (max->_right) max = max->_right;
			_node = max;
		}
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_left)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}

			_node = parent;
		}
		return *this;
	}

};


template<class K ,class T,class KeyOfT>
struct RBTree
{

	typedef RBTreeNode<T> Node;
	typedef _RBTreeIterator<T, T&, T*> iterator;
	typedef _RBTreeIterator<T,const T&,const T*> const_iterator;

public:
	iterator begin()
	{
		Node* left = _root;
		while (left && left->_left)left = left->_left;
		return iterator(left);
	}	
	
	const_iterator begin() const
	{
		Node* left = _root;
		while (left && left->_left)left = left->_left;
		return const_iterator(left);
	}

	iterator end() { return iterator(nullptr); }
	const_iterator end() const  { return const_iterator(nullptr); }

	pair<iterator,bool> Insert(const T& data)
	{
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root), true);
		}

		KeyOfT kot;
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (kot(cur->_data) < kot(data)) { parent = cur; cur = cur->_right; }
			else if (kot(cur->_data) > kot(data)) { parent = cur; cur = cur->_left; }
			else return make_pair(iterator(cur), false);
		}

		//新增子结点
		cur = new Node(data);
		Node* newnode = cur;
		cur->_col = RED;
		if (kot(parent->_data) < kot(data)) { parent->_right = cur; cur->_parent = parent; }
		else { parent->_left = cur; cur->_parent = parent; }

		//调整树
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;

			if (parent == grandfather->_left)
			{
				Node* uncle = grandfather->_right;
				//情况1：uncle存在且为红
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					cur = grandfather;
					parent = cur->_parent;
				}
				else//uncle不存在或者为黑
				{
					if (cur == parent->_left)//情况2：右单旋
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else//情况3：先左单旋再右单旋
					{
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}

			}
			else//parent == grandfather->_right
			{
				Node* uncle = grandfather->_left;
				//情况1：uncle存在且为红
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;
					cur = grandfather;
					parent = cur->_parent;
				}
				else//uncle不存在或者为黑
				{
					//   g
					//      f
					//           c
					//
					if (cur == parent->_right)
					{
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//   g
						//      f
						//   c
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
		}


		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		//重新链接两处子树
		parent->_right = subRL;
		if (subRL) subRL->_parent = parent;

		Node* ppnode = parent->_parent;
		subR->_left = parent;
		parent->_parent = subR;

		//处理头结点关系
		if (ppnode == nullptr) 
		{ 
			_root = subR; 
			_root->_parent = nullptr; 
		}
		else
		{
			if (ppnode->_left == parent) ppnode->_left = subR;
			else ppnode->_right = subR;

			subR->_parent = ppnode;
		}
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		//重新链接两处子树
		parent->_left = subLR;
		if (subLR) subLR->_parent = parent;

		Node* ppnode = parent->_parent;
		subL->_right = parent;
		parent->_parent = subL;

		//处理头结点关系
		if (ppnode == nullptr)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent) ppnode->_left = subL;
			else ppnode->_right = subL;

			subL->_parent = ppnode;
		}
	}

	void Inorder()
	{
		_Inorder(_root);
	}

	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;

		_Inorder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_Inorder(root->_right);
	}


private:
	Node* _root = nullptr;
};

