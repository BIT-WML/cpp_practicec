#include <stdio.h>
#include <assert.h>
//模拟实现strlen()
//int strlen(char* str)
//{
//	int cnt = 0;
//	while (*str != '\0')
//	{
//		cnt++;
//		str++;
//	}
//	return cnt;
//}

//strlen递归版
int strlen(char* str)
{
	if (*str == '\0')
		return 0;
	str++;
	return strlen(str) + 1;
}

//模拟实现strcpy
char* strcpy(char* dest, const char* src)
{
	char* ret = dest;
	assert(dest != NULL);
	assert(src != NULL);
	while((*dest++ = *src++))
	{ }
	return ret;
}

//模拟实现strcat
char* strcat(char* dest, const char* src)
{
	char* ret = dest;
	assert(dest != NULL);
	assert(src != NULL);
	while(*dest !='\0')
	{
		dest++;
	}
	while(*dest++ = *src++)
	{ }
	return ret;
}

//模拟实现strstr
char* strstr(const char* str1, const char* str2)
{
	char* cur = str1;
	char* s1, *s2;
	assert(str1 != NULL);
	if (str2 == NULL)
		return "str2 is NULL";

	while (*cur)
	{
		s1 = cur;
		s2 = (char*)str2;
		while (*s1 && *s2 && *s1 == *s2)
		{
			s1++;
			s2++;
		}
		if (*s2 == NULL)
			return cur;
		cur++;
	}
	return "not found";
}

//模拟实现strcmp
int strcmp(const char* str1, const char* str2)//此处只比较至最短字符串的尾部
{
	int ret = 0;
	assert(str1 != NULL);
	assert(str2 != NULL);
	while((*str1-*str2) == 0 && *str2 && *str1)
	{
		str1++;
		str2++;
	}
	if (ret < 0)
		ret = -1;
	else if (ret > 0)
		ret = 1;
	return ret;
}

//模拟实现memcpy -》 适用于内存不重叠的情况
void* memcpy(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest != NULL);
	assert(src != NULL);
	while (num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	*(char*)dest = '\0';
	return ret;
}

//模拟实现memmove -》 适用于内存有重叠的情况
// dest[1,10]   src[11,20] -> 从头或尾开始拷贝都可以
// dest[11,20]  src[1,10]  ->从头或尾开始拷贝都可以
// dest[1,10]   src[5,14]  -> 从头开始拷贝，从尾开始拷贝会使src头部的数据被覆盖
// dest[5,14]   src[1,10] ->  从尾开始拷贝。从头开始拷贝会使src尾部的数据被覆盖
void* memmove(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	if (dest <= src || (char*)dest >= (char*)src + num)
	{
		//从头开始拷贝
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		//从尾部开始拷贝
		dest = (char*)dest + num - 1;
		src = (char*)src + num - 1;
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest - 1;
			src = (char*)src - 1;
		}
	}
	return ret;
}

int main()
{
	char s[] = "wml is a handsome people!";
	char tmp[64];
	//strcpy(tmp, s);
	//strcat(tmp, s);
	//printf("%s\n", tmp);
	//printf("%s\n", strstr(tmp, "people"));
	//printf("%d\n", strcmp(tmp, s));
	//printf("%d\n", strlen(s));
	printf("%s\n", memcpy(tmp, s, 20));
	printf("%s\n", memmove(tmp, s, 20));
}