#include<stdio.h>
#include<stdbool.h>
#include<math.h>

//1-日历中的数字
//每组输入一行，有 3 个数字 y,m,x(1000<=y<=3000,1<=m<=12,0<=x<=9)，分别代表年份，月
//份，和他想知道哪个数字出现的次数。

//判断是否闰年
int is_leap(int year)
{
	if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
		return 1;
	else
		return 0;
}

//int main()
//{
//	int y, m, x, sum = 0, count = 0;
//	int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//	while (scanf("%d %d %d", &y, &m, &x) != EOF)
//	{
//		//判断是否闰年
//		if (is_leap(y) == 1)
//			arr[2] = 29;
//		else
//			arr[2] = 28;
//
//		//判断年份中是否有数字x，并统计次数
//		int year = y;
//		while (year)
//		{
//			if (year % 10 == x)
//				count++;
//			year /= 10;
//		}
//		sum += count * (*(arr + m));
//		count = 0;
//
//		//统计月份中数字x的次数
//		//特殊情况，x为0时
//		if (x == 0)
//		{
//			if (m > 0 && m <= 10)
//				count = 1;
//			else
//				count = 0;
//
//			sum += count * (*(arr + m));
//			count = 0;
//		}
//		else
//		{
//			int mouth = m;
//			while (mouth)
//			{
//				if (mouth % 10 == x)
//					count++;
//				mouth /= 10;
//			}
//			sum += count * (*(arr + m));
//			count = 0;
//		}
//
//		//判断每日会出现x的次数
//		for (int i=1; i <= *(arr + m); i++)
//		{
//			int everyday = i;
//			if (i < 10)
//			{
//				if (i == x)
//					sum++;
//				if (x == 0)
//					sum++;
//			}
//			//else if((i==10 || i==20 || i==30) &&(x==1 || x==0))
//			//{
//			//	sum++;
//			//}
//			else
//			{
//				while (everyday % 10 == x)
//				{
//					sum++;
//					everyday /= 10;
//				}
//				//	sum++;
//				//everyday /= 10;
//			}
//		}
//		printf("%d\n", sum);
//		sum = 0, count = 0;
//	}
//
//	return 0;
//}



//2-数字统计
//输入共 1 行，为两个正整数 L 和 R，之间用一个空格隔开。1≤L≤R≤10000
//int main()
//{
//	int L, R;
//	while (scanf("%d %d", &L, &R) != EOF)
//	{
//		int sum = 0;
//		for (int i = L; i <= R; i++)
//		{
//			int n = i;
//			while (n)
//			{
//				if (n % 10 == 2)
//				{
//					sum++;
//				}
//				n /= 10;
//			}
//		}
//		printf("%d\n", sum);
//	}
//}



//3-约瑟夫环
//输入一行包含三个整数 n,k,m
//1 <= n <= 100, 1 <= k <= n - 1, 1 <= m <= 100




//4-素数分布
//第一行一个整数 T(T≤1000)，表示数据的组数。
//接下来一共 T 行，第 i + 1(1≤i≤T)行表示第 i 组数据。每行一个正整数 n(n≤1000)。
int main()
{
	int T;
	bool number[1001];
	memset(number, true, sizeof(number));
	scanf("%d", &T);
	while (T--)
	{
		int n,count=0;
		scanf("%d", &n);
		for (int i = 2; i <= sqrt(n); i++)
		{
			if (number[i]==true)
			{
				for (int j = 2; j * i <= n; j++)
					number[i * j] = false;
			}
		}

		for (int i = 2; i <= n; i++)
			if (number[i]==true)
				count++;

		printf("%d", count);
	}
}