#include<iostream>
using namespace std;

//class Time
//{
//public:
//    Time(int hour = 0)
//        :_hour(hour)
//    {
//        cout << "Time()" << endl;
//    }
//private:
//        int _hour;
//};
//class Date
//{
//public:
//    Date(int day)
//    {}
//private:
//    int _day;
//    Time _t;
//};
//int main()
//{
//    Date d(1);
//}



//class A
//{
//public:
//    A(int a)
//        :_a1(a)
//        , _a2(_a1)
//    {}
//
//    void Print() {
//        cout << _a1 << " " << _a2 << endl;
//    }
//private:
//    int _a2;
//    int _a1;
//};
//int main() {
//    A aa(1);
//    aa.Print();
//}


//class A
//{
//public:
//	A() { ++_scount; }
//	A(const A& t) { ++_scount; }
//	~A() { --_scount; }
//	static int GetACount() { return _scount; }
//private:
//	static int _scount;
//};
//
//int A::_scount = 0;
//
//void main()
//{
//	cout << A::GetACount() << endl;
//	A a1, a2;
//	A a3(a1);
//	cout << A::GetACount() << endl;
//}






class A
{
public:
	A(int a = 0)
		:_a(a)
	{
		cout << "A(int a)" << endl;
	}
	A(const A& aa)
		:_a(aa._a)
	{
		cout << "A(const A& aa)" << endl;
	}
	A& operator=(const A& aa)
	{
		cout << "A& operator=(const A& aa)" << endl;
		if (this != &aa)
		{
			_a = aa._a;
		}
		return *this;
	}
	~A()
	{
		cout << "~A()" << endl;
	}
private:
	int _a;
};
void f1(A aa)
{}
A f2()
{
	A aa;
	return aa;
}
//int main()
//{
//	// 传值传参
//	A aa1;
//	f1(aa1);
//	cout << endl;
//	// 传值返回
//	f2();
//	cout << endl;
//	// 隐式类型，连续构造+拷贝构造->优化为直接构造
//	f1(1);
//	// 一个表达式中，连续构造+拷贝构造->优化为一个构造
//	f1(A(2));
//	cout << endl;
//	// 一个表达式中，连续拷贝构造+拷贝构造->优化一个拷贝构造
//	A aa2 = f2();
//	cout << endl;
//	// 一个表达式中，连续拷贝构造+赋值重载->无法优化
//	aa1 = f2();
//	cout << endl;
//	return 0;
//}


class Date {
public:
	Date(int year,int month,int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	ostream& operator<<(ostream& out)
	{
		out << _year << "-" << _month << "-" << _day;
		return out;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	//Date d1(2022, 12, 1);
	//d1 << cout;
	//cout << d1;

	int a = 10;
	int b = 1;
	const int* p = &a;
	p = &b;     //p可以改
	//*p = 20;	//p指向的内容不能改

	int* const pp = &a;
	//pp = &b;    //pp不能改
	*pp = 20;     //pp指向的内容可以改
}