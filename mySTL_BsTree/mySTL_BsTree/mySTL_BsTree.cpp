﻿#include"mySTL_BsTree.hpp"


int main()
{
	myBsTree::BsTree<int> b;
	b.InsertR(10);
	b.InsertR(1);
	b.InsertR(7);
	b.InsertR(3);
	b.InsertR(36);
	b.InsertR(6);
	b.InOrder();
	b.Erase(10);
	b.EraseR(36);
	std::cout << b.FindR(3) << std::endl;
	b.InOrder();

}