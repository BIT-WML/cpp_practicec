#pragma once
#include<iostream>
using namespace std;

namespace myBsTree
{
	template<class K>
	struct BsTreeNode {
		BsTreeNode<K>* _left;
		BsTreeNode<K>* _right;
		K _key;
		
		BsTreeNode(const K& key):_left(nullptr),_right(nullptr),_key(key){}
	};

	template<class K>
	class BsTree
	{
		typedef BsTreeNode<K> Node;

	public:
		BsTree():_root(nullptr){}
		~BsTree() { Destroy(_root); _root = nullptr; }
		BsTree(const BsTree<K>& k) { _root = Copy(k._root); }
		BsTree<K>& operator=(BsTree<K> k) { swap(_root, k._root); return *this; }
		void InOrder() { _InOrder(_root); cout << endl; }
		bool InsertR(const K& key) { return _InsertR(_root, key); }
		bool FindR(const K& key) { return _FindR(_root, key); }
		bool EraseR(const K& key) { return _EraseR(_root, key); }

		bool Insert(const K& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}

			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else return false;
			}

			cur = new Node(key);
			if (parent->_key < key)
				parent->_right = cur;
			else
				parent->_left = cur;

			return true;
		}

		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)	cur = cur->_right;
				else if (cur->_key > key) cur = cur->_left;
				else return true;

				return false;
			}
		}

		bool Erase(const K& key)
		{
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key< key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					//1.左右为空，直接删除
					//2.左为空或右为空
					//3.左右都不为空
					if (cur->_left == nullptr)
					{
						if (cur == _root)
							_root = cur->_right;
						else
						{
							//需要判断parent是否为空，因为cur为root时，parent为空
							if (parent->_left == cur)//判断要删除的结点是上一个结点的左结点还是右节点
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}
						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
							_root = cur->_left;
						else
						{
							if (parent->_left == cur)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
					}
					else
					{
						parent = cur;
						Node* minRight = cur->_right;
						while (minRight->_left)
						{
							parent = minRight;
							minRight = minRight->_left;
						}

						cur->_key = minRight->_key;//用赋值代替修改
						if (minRight == parent->_left)//链接minRight的右边结点（可为空）,因为左边结点必为空
							parent->_left = minRight->_right;
						else
							parent->_right = minRight->_right;
						delete minRight;
					}
					return true;

				}
			}
			return false;
		}



	private:
		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}

		void Destroy(Node* root)
		{
			if (root == nullptr) return;
			Destroy(root->_left);
			Destroy(root->_right);
			delete root;
		}

		Node* Copy(Node* root)
		{
			if (root == nullptr) return nullptr;
			Node* newnode = new Node(root->_key);
			newnode->_left = Copy(root->_left);
			newnode->_right = Copy(root->_right);

			return newnode;
		}

		 bool _InsertR(Node*& root, const K& key)
		{
			if (root == nullptr)
			{
				root = new Node(key);
				return true;
			}

			if (root->_key < key) return _InsertR(root->_right, key);
			else if (root->_key > key) return _InsertR(root->_left, key);
			else return false;
		}

		bool _FindR(Node* root, const K& key)
		{
			if (root == nullptr) return false;
			if (root->_key < key)
				return _FindR(root->_right, key);
			else if (root->_key > key)
				return _FindR(root->_left, key);
			else return true;
		}

		bool _EraseR(Node*& root, const K& key)
		{
			if (root == nullptr) return false;
			if (root->_key > key) return _EraseR(root->_left, key);
			else if (root->_key < key) return _EraseR(root->_right, key);
			else
			{
				Node* del = root;
				if (root->_right == nullptr)
					root = root->_left;
				else if (root->_left == nullptr)
					root = root->_right;
				else
				{
					Node* minRight = root->_right;
					while (minRight->_left)
						minRight = minRight->_left;
					swap(minRight->_key, root->_key);
					return _EraseR(root->_right, key);
				}
				delete del;
				return true;
			}
		}

	private:
		Node* _root = nullptr;
	};
}