#pragma once
#include<iostream>
#include<assert.h>
using namespace std;


namespace myList
{
	template<class T>
	struct list_node
	{
		list_node<T>* _next;
		list_node<T>* _prev;
		T _data;

		list_node(const T& x)
			:_prev(nullptr)
			, _next(nullptr)
			, _data(x)
		{}
	};

	template<class T, class Ref, class Ptr>
	struct _list_iterator
	{
		typedef list_node<T> node;
		typedef _list_iterator<T, Ref, Ptr> Self;
		node* _pnode;

		_list_iterator(node* p)
			:_pnode(p)
		{}

		Ptr operator->() { return &_pnode->_data; }
		Ref operator*() { return _pnode->_data; }
		Self& operator++() { _pnode = _pnode->_next; return *this; }
		Self& operator++(int) { Self tmp(*this); _pnode = _pnode->_next; return tmp; }
		Self& operator--() { _pnode = _pnode->_prev; return *this; }
		Self& operator--(int) { Self tmp(*this); _pnode = _pnode->_prev; return tmp; }
		bool operator!=(const Self& it)const { return _pnode != it._pnode; }
		bool operator == (const Self& it)const { return _pnode == it._pnode; }
	};

	template<class T>
	class list
	{

	public:
		typedef list_node<T> node;

		typedef _list_iterator<T, T&, T*>iterator;
		typedef _list_iterator<T, const T&, const T*>const_iterator;

	private:
		node* _head;
		size_t _size;

	public:

		iterator begin() { return iterator(_head->_next); }
		iterator end() { return iterator(_head); }
		const_iterator begin()const { return const_iterator(_head->_next); }
		const_iterator end()const { return const_iterator(_head); }

		list() { initialize(); }
		void swap(list<T>& it) { std::swap(_head, it._head); std::swap(_size, it._size); }
		list<T>& operator=(list<T> it) { swap(it); return *this; }
		size_t size()const { return _size; }
		bool empty()const { return _size == 0; }
		~list() { clear(); delete _head; _head = nullptr; }
		void push_back(const T& x) { insert(end(), x); }
		void push_front(const T& x) { insert(begin(), x); }
		void pop_back() { erase(--end()); }
		void pop_front() { erase(begin()); }

		void initialize()
		{
			_head = new node(T());
			_head->_next = _head;
			_head->_prev = _head;
			_size = 0;
		}

		template <class InputIterator>
		list(InputIterator first, InputIterator last)
		{
			initialize();
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		list(const list<T>& it)
		{
			initialize();
			list<T> tmp(it.begin(), it.end());
			swap(tmp);
		}

		void clear()
		{
			iterator it = begin();
			while (it != end()) it = erase(it);
		}

		iterator erase(iterator pos)
		{
			assert(pos != end());
			node* prev = pos._pnode->_prev;
			node* next = pos._pnode->_next;

			prev->_next = next;
			next->_prev = prev;

			delete pos._pnode;
			--_size;

			return iterator(next);
		}

		iterator insert(iterator pos, const T& x)
		{
			node* newnode = new node(x);
			node* cur = pos._pnode;
			node* prev = cur->_prev;

			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;

			++_size;
			return iterator(newnode);
		}


	};
}


//////////////////////////////////////////////////////////////////////////////////////////
