﻿#include"mySTL_L.h"
//#include<list>

int main()
{
	myList::list<int> l1;
	l1.push_back(1);
	l1.push_back(2);
	l1.push_back(3);
	l1.push_back(4);
	l1.push_back(5);

	l1.push_back(10);
	l1.push_front(100);
	l1.pop_back();
	l1.pop_front();

	myList::list<int> l2(l1);

	myList::list<int>::iterator it = l2.begin();
	while (it != l2.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	

	return 0;
}
