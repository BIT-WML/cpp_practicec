#pragma once
#include<vector>
#include<string>
#include<iostream>
using namespace std;

namespace hashbucket{

	template<class K>
	struct HashFunc {
		size_t operator()(const K& key) { return (size_t)key; }

	};

	//模板的特化
	template<>
	struct HashFunc<string> {
		size_t operator()(const string& key)
		{
			size_t hashindex = 0;
			for (auto ch : key)
			{
				hashindex *= 123;
				hashindex += ch;
			}
			return hashindex;
		}
	};


template<class T>
struct HashNode
{

	T _data;
	HashNode<T>* _next;

	HashNode(const T& data)
		:_data(data)
		, _next(nullptr)
	{}
};

// 前置声明
template<class K, class T, class Hash, class KeyOfT>
class HashTable;

// 为什么const迭代器没有复用?
template<class K, class T, class Ref, class Ptr, class Hash, class KeyOfT>
struct __HTIterator
{
	typedef HashNode<T> Node;
	typedef __HTIterator<K, T, Ref, Ptr, Hash, KeyOfT> Self;

	typedef HashTable<K, T, Hash, KeyOfT> HT;

	Node* _node;
	HT* _ht;

	__HTIterator(Node* node, HT* ht)
		:_node(node)
		, _ht(ht)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator != (const Self& s) const
	{
		return _node != s._node;
	}

	Self& operator++()
	{
		if (_node->_next)
		{
			_node = _node->_next;
		}
		else
		{
			// 当前桶走完了，要找下一个桶的第一个
			KeyOfT kot;
			Hash hash;
			size_t hashi = hash(kot(_node->_data)) % _ht->_tables.size();
			++hashi;
			while (hashi < _ht->_tables.size())
			{
				if (_ht->_tables[hashi])
				{
					_node = _ht->_tables[hashi];
					break;
				}
				else
				{
					++hashi;
				}
			}

			// 后面没有桶了
			if (hashi == _ht->_tables.size())
				_node = nullptr;
		}

		return *this;
	}
};

template<class K, class T, class Hash, class KeyOfT>
class HashTable
{
	typedef HashNode<T> Node;

	template<class K, class T, class Ref, class Ptr, class Hash, class KeyOfT>
	friend struct __HTIterator;

public:
	typedef __HTIterator<K, T, T&, T*, Hash, KeyOfT> iterator;
	typedef __HTIterator<K, T, const T&, const T*, Hash, KeyOfT> const_iterator;


	iterator begin()
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			if (_tables[i])
			{
				return iterator(_tables[i], this);
			}
		}

		return iterator(nullptr, this);
	}

	iterator end()
	{
		return iterator(nullptr, this);
	}

	const_iterator begin() const
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			if (_tables[i])
			{
				return const_iterator(_tables[i], this);
			}
		}

		return const_iterator(nullptr, this);
	}

	const_iterator end() const
	{
		return const_iterator(nullptr, this);
	}

	HashTable()
		:_n(0)
	{
		//_tables.resize(10);
		_tables.resize(__stl_next_prime(0));
	}

	~HashTable()
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			// 释放桶
			Node* cur = _tables[i];
			while (cur)
			{
				Node* next = cur->_next;
				delete cur;
				cur = next;
			}

			_tables[i] = nullptr;
		}
	}

	pair<iterator, bool> Insert(const T& data)
	{
		KeyOfT kot;

		iterator it = Find(kot(data));
		if (it != end())
			return make_pair(it, false);

		// 负载因子控制在1，超过就扩容
		if (_tables.size() == _n)
		{
			/*HashTable<K, V, Hash> newHT;
			newHT._tables.resize(_tables.size() * 2);
			for (auto cur : _tables)
			{
			while (cur)
			{
			newHT.Insert(cur->_kv);
			cur = cur->_next;
			}
			}
			_tables.swap(newHT._tables);*/

			vector<Node*> newTables;
			//newTables.resize(2 * _tables.size(), nullptr);
			newTables.resize(__stl_next_prime(_tables.size()), nullptr);

			for (size_t i = 0; i < _tables.size(); ++i)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;

					size_t hashi = Hash()(kot(cur->_data)) % newTables.size();
					// 头插到新表
					cur->_next = newTables[hashi];
					newTables[hashi] = cur;

					cur = next;
				}

				_tables[i] = nullptr;
			}

			_tables.swap(newTables);
		}

		size_t hashi = Hash()(kot(data)) % _tables.size();
		// 头插
		Node* newnode = new Node(data);
		newnode->_next = _tables[hashi];
		_tables[hashi] = newnode;
		++_n;

		return  make_pair(iterator(newnode, this), true);
	}

	iterator Find(const K& key)
	{
		KeyOfT kot;
		size_t hashi = Hash()(key) % _tables.size();
		Node* cur = _tables[hashi];
		while (cur)
		{
			if (kot(cur->_data) == key)
			{
				return iterator(cur, this);
			}
			else
			{
				cur = cur->_next;
			}
		}

		return end();
	}

	bool Erase(const K& key)
	{
		size_t hashi = Hash()(key) % _tables.size();
		Node* prev = nullptr;
		Node* cur = _tables[hashi];
		while (cur)
		{
			if (cur->_kv.first == key)
			{
				// 准备删除
				if (cur == _tables[hashi])
				{
					_tables[hashi] = cur->_next;
				}
				else
				{
					prev->_next = cur->_next;
				}

				delete cur;
				--_n;

				return true;
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}

		return false;
	}

	inline unsigned long __stl_next_prime(unsigned long n)
	{
		static const int __stl_num_primes = 28;
		static const unsigned long __stl_prime_list[__stl_num_primes] =
		{
			53, 97, 193, 389, 769,
			1543, 3079, 6151, 12289, 24593,
			49157, 98317, 196613, 393241, 786433,
			1572869, 3145739, 6291469, 12582917, 25165843,
			50331653, 100663319, 201326611, 402653189, 805306457,
			1610612741, 3221225473, 4294967291
		};

		for (int i = 0; i < __stl_num_primes; ++i)
		{
			if (__stl_prime_list[i] > n)
			{
				return __stl_prime_list[i];
			}
		}

		return __stl_prime_list[__stl_num_primes - 1];
	}

private:
	vector<Node*> _tables;  // 指针数组
	size_t _n = 0;


};

}