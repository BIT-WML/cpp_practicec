#include<iostream>
#include<string.h>

//int main()
//{
//	double x = 1.1, y = 2.2;
//	int&& rr1 = 10;
//	const double&& rr2 = x + y;
//	rr1 = 20;
//	//rr2 = 5.5;  // 报错
//	cout << &rr1 << endl;
//	return 0;
//}

/////////////////////////////////////////////
using namespace std;
namespace tes
{
	class string
	{
	public:
		typedef char* iterator;
		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
                   		{
			cout << "默认构造 -- string(char* str)" << endl;
 			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		// s1.swap(s2)
		void swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}

		// 拷贝构造
		string(const string& s)
		{

			string tmp(s._str);
			swap(tmp);
			cout << "拷贝构造 -- string(const string& s) " << endl;

		}

		// 赋值重载
		//string& operator=(string s)
		//{
		//	cout << "赋值重载 -- string& operator=(string s) " << endl;
		//	//string tmp(s);
		//	//swap(tmp);
		//	swap(s);

		//	return *this;
		//}

		// 移动构造
		string(string&& s) noexcept
		{
			cout << "string(const string& s) -- 移动拷贝" << endl;

			swap(s);
		}

		// 移动赋值
		string& operator=(string&& s) noexcept
		{
			cout << "string& operator=(string&& s) -- 移动赋值" << endl;
			swap(s);

			return *this;
		}


		~string()
		{
			delete[] _str;
			_str = nullptr;
		}

		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
 				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}

		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}

			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}

		//string operator+=(char ch)
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
 		char* _str = nullptr;
		size_t _size = 0;
		size_t _capacity = 0; // 不包含最后做标识的\0
	};

	string to_string(int value)
	{
		bool flag = true;
		if (value < 0)
		{
			flag = false;
			value = 0 - value;
		}

		tes::string str;
		while (value > 0)
		{
			int x = value % 10;
			value /= 10;

			str += ('0' + x);
		}

		if (flag == false)
		{
			str += '-';
		}

		std::reverse(str.begin(), str.end());
		return str;
	}
}

//int main()
//{
// //	tes::string ret;
//	//ret = tes::to_string(-1234);
//
//	//tes::string re = tes::to_string(-1234);
//
//	tes::string s1("hello world");
//	tes::string s2(s1);
//	tes::string s3(move(s1));
//
//	return 0;
//}

//int main()
//{
//	list<tes::string> lt;
//	tes::string s1("111111");
//	lt.push_back(move(s1));
//
//	lt.push_back(tes::string("222222"));
//
//	lt.push_back("333333");
//
//	return 0;
//}

//int main()
//{
//	double x = 1.1, y = 2.2;
//	int&& rr1 = 10;
//	const double&& rr2 = x + y;
//
//	rr1++;
//	//rr2++;
//
//	cout << &rr1 << endl;
//	cout << &rr2 << endl;
//
//	return 0;
//}


void Fun(int& x) { cout << "左值引用" << endl; }
void Fun(const int& x) { cout << "const 左值引用" << endl; }
void Fun(int&& x) { cout << "右值引用" << endl; }
void Fun(const int&& x) { cout << "const 右值引用" << endl; }


//template<typename T>
//void PerfectForward(T&& t)
//{
//	Fun(t);
//}

// std::forward<T>(t)在传参的过程中保持了t的原生类型属性。
template<typename T>
void PerfectForward(T&& t)
{
	Fun(std::forward<T>(t));
}

int main()
{
	PerfectForward(10);           // 右值
	int a;
	PerfectForward(a);            // 左值
	PerfectForward(std::move(a)); // 右值
	const int b = 8;
	PerfectForward(b);      // const 左值
	PerfectForward(std::move(b)); // const 右值
	return 0;
}
