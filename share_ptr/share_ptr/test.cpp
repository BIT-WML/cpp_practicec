#include"share_ptr.h"
#include<iostream>
#include<memory>
using namespace std;

class Date {
private:
	int year_;
	int month_;
	int day_;
public:
	Date(int year = 2024, int month = 1, int day =1)
		:year_(year), month_(month), day_(day)
	{}
};

//int main()
//{
// // 	auto_ptr<Date> d1(new Date(2008, 1, 1));
//	//auto_ptr<Date> d2(d1);
//	//auto_ptr<Date> d3 = d2;
//
//
//	//auto_ptr<int[]> d4(new int[12]);
//	//unique_ptr<int[]> d5(new int[12]);
//	//unique_ptr<Date> d3 = std::make_unique<Date>();
//
//	unique_ptr<Date> d1(new Date(2008, 1, 1));
//	unique_ptr<Date> d2(d1);
//	unique_ptr<Date> d3 = d1;
//   	return 0;
// }

//做返回值时可复制
//std::unique_ptr<int> func(int val)
//{
//	std::unique_ptr<int> up(new int(val));
//	return up;
//}
//
//int main()
//{
//	std::unique_ptr<int> sp1 = func(123);
//
//	return 0;
//}

//移动语义
//#include <memory>
//
//int main()
//{
//	std::unique_ptr<int> sp1(std::make_unique<int>(123));
//
//	std::unique_ptr<int> sp2(std::move(sp1));
//
//	std::unique_ptr<int> sp3;
//	sp3 = std::move(sp2);
//
//	return 0;
//}


//自定义智能指针对象持有的资源的释放函数
//#include <iostream>
//#include <memory>
//
//class Socket
//{
//public:
//    Socket() {}
//    ~Socket() {}
//    //关闭资源句柄
//    void close() {}
//
//};
//
//int main()
//{
//    auto deletor = [](Socket* pSocket) {
//        //关闭句柄
//        pSocket->close();
//        //TODO: 你甚至可以在这里打印一行日志...
//        delete pSocket;
//        };
//
//    std::unique_ptr<Socket, void(*)(Socket* pSocket)> spSocket(new Socket(), deletor);
//    return 0;
//}

//int main()
//{
//        //初始化方式1
//	std::shared_ptr<Date> sp1(new Date());
//	std::shared_ptr<Date> sp2(sp1);
//	std::shared_ptr<Date> sp3 = sp2;
//
//    std::cout << "use count: " << sp1.use_count() << std::endl;
//
//    return 0;
//}

//int main()
//{
//        //初始化方式1
//	myptr::shared_ptr<Date> sp1(new Date());
//	myptr::shared_ptr<Date> sp2(sp1);
//	myptr::shared_ptr<Date> sp3 = sp2;
//
//    std::cout << "use count: " << sp1.use_count() << std::endl;
//
//	std::shared_ptr<Date> s1(new Date());
//	std::shared_ptr<Date> s2(s1);
//	std::shared_ptr<Date> s3 = s2;
//
//    std::cout << "use count: " << s1.use_count() << std::endl;
//
//    return 0;
//}

//




// test_std_enable_shared_from_this.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//#include <iostream>
//#include <memory>
//
//class A : public std::enable_shared_from_this<A>
//{
//public:
//    A()
//    {
//        m_i = 9;
//        //注意:
//        //比较好的做法是在构造函数里面调用shared_from_this()给m_SelfPtr赋值
//        //但是很遗憾不能这么做,如果写在构造函数里面程序会直接崩溃
//        std::cout << "A constructor" << std::endl;
//    }
//
//    ~A()
//    {
//        m_i = 0;
//        std::cout << "A destructor" << std::endl;
//    }
//
//    void func()
//    {
//        m_SelfPtr = shared_from_this();
//        cout << m_SelfPtr.use_count() << endl;
//    }
//
//public:
//    int                 m_i;
//    std::weak_ptr<A>  m_SelfPtr;
//
//};
//
//int main()
//{
//    {
//        std::shared_ptr<A> spa(new A());
//        spa->func();
//    }
//
//    return 0;
//}




//#include <iostream>
//#include <memory>
//
//int main()
//{
//    //创建一个std::shared_ptr对象
//    std::shared_ptr<int> sp1(new int(123));
//    std::cout << "use count: " << sp1.use_count() << std::endl;
//
//    //通过构造函数得到一个std::weak_ptr对象
//    std::weak_ptr<int> sp2(sp1);
//    std::cout << "use count: " << sp1.use_count() << std::endl;
//
//    //通过赋值运算符得到一个std::weak_ptr对象
//    std::weak_ptr<int> sp3 = sp1;
//    std::cout << "use count: " << sp1.use_count() << std::endl;
//
//    //通过一个std::weak_ptr对象得到另外一个std::weak_ptr对象
//    std::weak_ptr<int> sp4 = sp2;
//    std::cout << "use count: " << sp1.use_count() << std::endl;
//
//    return 0;
//}

//int main()
//{
//	cout << sizeof(unique_ptr<int>) << endl;
//	cout << sizeof(shared_ptr<int>) << endl;
//	cout << sizeof(weak_ptr<int>) << endl;
//}

void print()
{
    cout << "print" << endl;
}

int main()
{
    //创建一个std::shared_ptr对象
    //myptr::shared_ptr<int,void (*)()> sp1(new int(123),print);
    myptr::shared_ptr<int> sp1(new int(123));
    std::cout << "use count: " << sp1.use_count() << std::endl;

    //通过构造函数得到一个std::weak_ptr对象
    myptr::weak_ptr<int> sp2(sp1);
    std::cout << "use count: " << sp1.use_count() << std::endl;

    //通过赋值运算符得到一个std::weak_ptr对象
    myptr::weak_ptr<int> sp3 = sp1;
    std::cout << "use count: " << sp1.use_count() << std::endl;

    //通过一个std::weak_ptr对象得到另外一个std::weak_ptr对象
    myptr::weak_ptr<int> sp4 = sp2;
    std::cout << "use count: " << sp1.use_count() << std::endl;

    myptr::unique_ptr<int> sp5(new int(123));
    //myptr::unique_ptr<int> sp6 = sp5;

    return 0;
}