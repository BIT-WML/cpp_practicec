#include<iostream>
using namespace std;

class Time
{
public:
    Time()
    {
        _hour = 1;
        _minute = 1;
        _second = 1;
    }

    Time(const Time& t)
    {
        _hour = t._hour;
        _minute = t._minute;
        _second = t._second;
        cout << "Time::Time(const Time&)" << endl;
    }
private:
    int _hour;
    int _minute;
    int _second;
};

//class Date
//{
//private:
//    // 基本类型(内置类型)
//    int _year = 1970;
//    int _month = 1;
//    int _day = 1;
//    // 自定义类型
//    Time _t;
//};
//int main()
//{
//    Date d1;
//
//    // 用已经存在的d1拷贝构造d2，此处会调用Date类的拷贝构造函数
//    // 但Date类并没有显式定义拷贝构造函数，则编译器会给Date类生成一个默认的拷贝构造函数
//    Date d2(d1);
//    return 0;
//}


////////////////////////////////////////////////////////////////////////////////////

// 这里会发现下面的程序会崩溃掉？这里就需要我们以后讲的深拷贝去解决。
typedef int DataType;
class Stack
{
public:
    Stack(size_t capacity = 10)
    {
        _array = (DataType*)malloc(capacity * sizeof(DataType));
        if (nullptr == _array)
        {
            perror("malloc申请空间失败");
            return;
        }
        _size = 0;
        _capacity = capacity;
    }

    Stack(const Stack& st)
    {
        _array = (DataType*)malloc(sizeof(DataType) * st._capacity);
        if (_array == nullptr)
        {
            perror("malloc fail");
            return;
        }
        memcpy(_array, st._array, sizeof(int) * st._capacity);
        _capacity = st._capacity;
        _size = st._size;
    }

    void Push(const DataType& data)
    {
        // CheckCapacity();
        _array[_size] = data;
        _size++;
    }

    ~Stack()
    {
        if (_array)
        {
            free(_array);
            _array = nullptr;
            _capacity = 0;
            _size = 0;
        }
    }
private:
    DataType* _array;
    size_t _size;
    size_t _capacity;
};

//int main()
//{
//    Stack s1;
//    s1.Push(1);
//    s1.Push(2);
//    s1.Push(3);
//    s1.Push(4);
//    Stack s2(s1);
//    s2.Push(10);
//    return 0;
//}

/////////////////////////////////////////////////////////////
// 全局的operator==
//class Date
//{
//public:
//    Date(int year = 1900, int month = 1, int day = 1)
//    {
//        _year = year;
//        _month = month;
//        _day = day;
//    }
//    //private:
//    int _year;
//    int _month;
//    int _day;
//};
//// 这里会发现运算符重载成全局的就需要成员变量是公有的，那么问题来了，封装性如何保证？
//// 这里其实可以用我们后面学习的友元解决，或者干脆重载成成员函数。
//bool operator==(const Date& d1, const Date& d2)
//{
//    return d1._year == d2._year
//        && d1._month == d2._month
//        && d1._day == d2._day;
//}
//
//int main()
//{
//    Date d1(2018, 9, 26);
//    Date d2(2018, 9, 26);
//    cout << (d1 == d2) << endl;
//}


//class Date
//{
//public:
//    Date(int year = 1900, int month = 1, int day = 1)
//    {
//         _year = year;
//         _month = month;
//         _day = day;
//    }
//
//        // bool operator==(Date* this, const Date& d2)
//        // 这里需要注意的是，左操作数是this，指向调用函数的对象
//        bool operator==(const Date& d2)
//    {
//        return _year == d2._year
//        && _month == d2._month
//        && _day == d2._day;
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};
