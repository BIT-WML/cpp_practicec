#include<iostream>
#include<mutex>
#include<memory>
using namespace std;

//class HeapOnly {
//public:
//	static HeapOnly* CreatObject() { return new HeapOnly(); }
//private:
//	HeapOnly() {}
//	HeapOnly(const HeapOnly&) = delete;
//};

class HeapOnly {
public:
	HeapOnly() {}
	void Destory() { this->~HeapOnly(); }
private:
	~HeapOnly() {}
};

class StackOnly {
private:
	int _a;
public:
	void* operator new(size_t size) = delete;
	void operator delete(void* p) = delete;

	StackOnly() :_a(0) {}

};

//int main()
//{
	//StackOnly s1;
	////StackOnly s2 = new StackOnly();
	//static StackOnly s2;
//}


//int main()
//{
	//由于构造函数私有，所以不能直接new，也不能直接在栈上构造
	//HeapOnly* h1 = HeapOnly::CreatObject();

	//HeapOnly h(*h1);//拷贝被禁掉了
	//HeapOnly* h1 = new HeapOnly();
	//static HeapOnly h4;

	//delete h1;

	//HeapOnly* p1 = new HeapOnly();
	//HeapOnly* p2 = new HeapOnly();
	//HeapOnly h1;
	//static HeapOnly h2;
	//HeapOnly h3(*p1);


	//return 0;
//}

//懒汉模式version1 -> 问题1：线程安全  问题2：内存泄露
//class Singleton {
//public:
//	static Singleton* getInstance()
//               	{
//		if (_instance == nullptr)
//			_instance = new Singleton();
//		return _instance;
//	}
//	~Singleton() { cout << "析构函数" << endl; }
//
//public:
//	// 实现一个内嵌垃圾回收类    
//	class CGarbo {
//	public:
//		~CGarbo() { 
//			if (Singleton::_instance != nullptr) { delete Singleton::_instance; }
//		}
//	};
//	// 定义一个静态成员变量，程序结束时，系统会自动调用它的析构函数从而释放单例对象
//	static CGarbo Garbo;
//
//private:
//	static Singleton* _instance;
//	Singleton() { cout << "构造函数" << endl; }
//	Singleton(const Singleton& s) = delete;
//	Singleton& operator=(const Singleton&) = delete;
//};
//Singleton* Singleton::_instance = nullptr;
//Singleton::CGarbo Garbo;

//懒汉模式version2 -> 不足：要求使用智能指针，锁有开销
//class Singleton {
//
//public:
//	typedef shared_ptr<Singleton> Ptr;
//	static Ptr getInstance()
//	{
//		if (_instance == nullptr)
//		{
//			std::lock_guard<std::mutex> _lck(_mtx);
//			if (_instance == nullptr)
//				_instance = shared_ptr<Singleton>(new Singleton());
//		}
//		return _instance;
//	}
//	~Singleton() { cout << "析构函数" << endl; }
//
//private:
//	static Ptr _instance;
//	static std::mutex _mtx;
//private:
//	Singleton() { cout << "构造函数" << endl; }
//	Singleton(const Singleton& s) = delete;
//	Singleton& operator=(const Singleton&) = delete;
//};
//Singleton::Ptr Singleton::_instance = nullptr;
//std::mutex Singleton::_mtx;


//懒汉模式version3
//class Singleton {
//private:
//	Singleton() { cout << "构造函数" << endl; }
//	~Singleton() { cout << "析构函数" << endl; }
//	Singleton(const Singleton& s) = delete;
//	Singleton& operator=(const Singleton&) = delete;
//public:
//	static Singleton& getInstance()
//	{
//		static Singleton _instance;
//		return _instance;
//	}
//};

//饿汉模式
class Singleton
{
private:
	static Singleton _instance;
private:
	~Singleton() { cout << "析构函数" << endl; }
	Singleton() { cout << "构造函数" << endl; }
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;
public:
	static Singleton& getInstance() { return _instance; }

};
Singleton Singleton::_instance;

int main()
{
	//Singleton* s1 = Singleton::getInstance();
	//Singleton* s2 = Singleton::getInstance();

	//Singleton::Ptr s1 = Singleton::getInstance();
	//Singleton::Ptr s2 = Singleton::getInstance();

	//Singleton& p1 = Singleton::getInstance();
	//Singleton& p2 = Singleton::getInstance();

	Singleton& s1 = Singleton::getInstance();
	//Singleton s4;
	Singleton& s2 = Singleton::getInstance();
	return 0;
}