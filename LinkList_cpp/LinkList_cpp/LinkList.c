#include"LinkList.h"

//打印结点
void PrintNode(LNode* plist)
{
	assert(plist);
	LNode* cur = plist;
	while (cur)
	{
		printf("%d->", cur->val);
		cur = cur->next;
	}
	printf("NULL\n");
}

//创建结点
LNode* BuyNode(ListData x)
{
	LNode* newnode = (LNode*)malloc(sizeof(LNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	newnode->next = NULL;
	newnode->val = x;
	return newnode;
}

//初始化
void InitList(LNode** pplist)
{
	assert(pplist);
	*pplist = BuyNode(0);
}

//头插
void InserFrontList(LNode* plist,ListData x)
{
	assert(plist);
	LNode* newnode = BuyNode(x);
	if (newnode == NULL)
	{
		perror("buynode fail");
		return;
	}
	newnode->next = plist->next;
	plist->next = newnode;
}

//尾插
void InsertBackList(LNode* plist, ListData x)
{
	assert(plist);
	LNode* cur = plist;
	while (cur->next)
	{
		cur = cur->next;
	}
	cur->next = BuyNode(x);
}

//删除查找的某个元素
void DelList(LNode* plist, ListData x)
{
	assert(plist);
	LNode* cur = plist;
	while (cur->next->val != x)
	{
		cur = cur->next;
		if (cur == NULL)
			return;
	}

	LNode* temp = cur->next;
	cur->next = cur->next->next;
	free(temp);
}

//在某个元素后插入元素
void InertList(LNode* plist, ListData pos, ListData x)
{
	assert(plist);
	LNode* cur = plist;
	while (cur->val != pos)
	{
		cur = cur->next;
		if (cur == NULL)
			return;
	}
	LNode* newnode = BuyNode(x);
	newnode->next = cur->next;
	cur->next = newnode;
}

//销毁链表
void DestroyList(LNode* plist)
{
	assert(plist);
	LNode* cur = plist;
	while (cur)
	{
		LNode* del = cur->next;
		free(cur);
		cur = del;
	}
}
