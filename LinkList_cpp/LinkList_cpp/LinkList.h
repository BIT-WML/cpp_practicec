#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int ListData;

typedef struct ListNode {
	struct ListNode* next;
	ListData val;
}LNode;

//初始化
//void InitList(LNode** plist);

//头插
void InsertFrontList(LNode* plist,ListData x);

//尾插
void InsertBackList(LNode* plist, ListData x);

//删除查找的某个元素
void DelList(LNode* plsit, ListData x);


//在某个元素后插入元素
void InertList(LNode* plist, ListData pos,ListData x);

//打印结点
void PrintNode(LNode* plist);

//创建结点
LNode* BuyNode(ListData x);

//销毁链表
void DestroyList(LNode* plist);
